#if !defined(_DEBUGGER_STUB_CPP)
#define _DEBUGGER_STUB_CPP

CUSTOM_COMMAND_SIG(debugger_jump_to_cursor)
CUSTOM_DOC("Debugger control is not yet supported on this platform.") {
    // stub
}

CUSTOM_COMMAND_SIG(debugger_add_breakpoint_at_cursor)
CUSTOM_DOC("Debugger control is not yet supported on this platform.") {
    // stub
}

CUSTOM_COMMAND_SIG(debugger_remove_breakpoint_at_cursor)
CUSTOM_DOC("Debugger control is not yet supported on this platform.") {
    // stub
}

CUSTOM_COMMAND_SIG(debugger_start_debugging)
CUSTOM_DOC("Debugger control is not yet supported on this platform.") {
    // stub
}

CUSTOM_COMMAND_SIG(debugger_stop_debugging)
CUSTOM_DOC("Debugger control is not yet supported on this platform.") {
    // stub
}

CUSTOM_COMMAND_SIG(debugger_continue_execution)
CUSTOM_DOC("Debugger control is not yet supported on this platform.") {
    // stub
}

CUSTOM_COMMAND_SIG(debugger_open_project_session)
CUSTOM_DOC("Debugger control is not yet supported on this platform.") {
    // stub
}

#endif // _DEBUGGER_STUB_CPP