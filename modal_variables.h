#if !defined(_MODAL_VARIABLES_H)
#define _MODAL_VARIABLES_H

global Command_Map_ID global_last_mapid   = 0;
global Command_Map_ID global_active_mapid = 0;

#define MOD_QUERY_CAP sizeof(command_space)
u8 mod_query_bar_space[MOD_QUERY_CAP] = {};

global b32 modal_inverted_theme_colors = false;

global b32 global_smooth_cursor = true;
global Rect_f32 global_cursor_rect = {};
global Rect_f32 global_last_cursor_rect = {};

global Range_i64 global_keyboard_macro_ranges[KeyCode_COUNT] = {};
global b32 global_keyboard_macro_is_dirty = false;
global u64 global_active_macro_code = 0;

global String_Const_u8 global_one_time_message = {};
global b32 global_show_one_time_message = false;

// Goto Storage {
global u8 goto_state_bar_space[MOD_QUERY_CAP] = {};
global Goto_State global_goto_state = {};
// }

global Query_State global_general_query = {};
global Query_State *active_query_state = 0;

// Search Storage {
global u8 search_state_search_string_space[MOD_QUERY_CAP]  = {};
global u8 search_state_replace_string_space[MOD_QUERY_CAP] = {};
global Search_State global_search_state = {};
// }

// CLI History {
global Query_State global_cli_query = {};
global Dll_Node_String_Const_u8 *cli_history_first = 0;
global Dll_Node_String_Const_u8 *cli_history = 0;
// }


#endif // _MODAL_VARIABLES_H