#if !defined(_MOD_CASE_CPP)
#define _MOD_CASE_CPP

CUSTOM_COMMAND_SIG(modal_set_chord_case)
CUSTOM_DOC("Activates 'case' chord.") {
    modal_set_active_mapid(app, chord_case);
    mod_show_bindings_hud(app);
}

function void modal_bind_chord_case(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(chord_case);
    ParentMap(mapid_vital);
    
    Bind(case_chord_lowercase,       KeyCode_L);
    Bind(case_chord_lowercase_token, KeyCode_L, KeyCode_Shift);
    Bind(case_chord_uppercase,       KeyCode_U);
    Bind(case_chord_uppercase_token, KeyCode_U, KeyCode_Shift);
    Bind(case_chord_camelcase,       KeyCode_C);
    Bind(case_chord_camelcase_token, KeyCode_C, KeyCode_Shift);
    
    Bind(modal_set_mode_normal, KeyCode_Escape);
}

CUSTOM_COMMAND_SIG(modal_set_theme_chord_case)
CUSTOM_DOC("Sets the theme for 'case' chord.") {
    MODAL_THEME_SET_COLOR(margin,        0, 0xff034449);
    MODAL_THEME_SET_COLOR(margin_hover,  0, 0xff021f4f);
    MODAL_THEME_SET_COLOR(margin_active, 0, 0xff055459);
    MODAL_THEME_COPY_COLOR(bar, 0, margin, 0);
    
    MODAL_THEME_SET_COLOR(cursor, 0, 0xff207469);
    MODAL_THEME_SET_COLOR(mark,   0, 0xff0d6695);
}

CUSTOM_COMMAND_SIG(case_chord_lowercase)
CUSTOM_DOC("See to_lowercase."){
    to_lowercase(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(case_chord_lowercase_token)
CUSTOM_DOC("See to_lowercase."){
    mod_select_token_forward(app);
    to_lowercase(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(case_chord_uppercase)
CUSTOM_DOC("See to_uppercase."){
    to_uppercase(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(case_chord_uppercase_token)
CUSTOM_DOC("See to_uppercase."){
    mod_select_token_forward(app);
    to_uppercase(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(case_chord_camelcase)
CUSTOM_DOC("See to_camelcase."){
    to_camelcase(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(case_chord_camelcase_token)
CUSTOM_DOC("See to_camelcase."){
    mod_select_token_forward(app);
    to_camelcase(app);
    modal_revert_mode(app);
}

#endif // _MOD_CASE_CPP