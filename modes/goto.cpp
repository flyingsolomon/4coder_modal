#if !defined(_MOD_GOTO_CPP)
#define _MOD_GOTO_CPP

CUSTOM_COMMAND_SIG(modal_set_mode_goto)
CUSTOM_DOC("Activates 'goto' mode.") {
    modal_set_active_mapid(app, mode_goto);
    Goto_State *state = &global_goto_state;
    state->view = get_active_view(app, Access_Read);
    
    i64 pos = view_get_cursor_pos(app, state->view);
    
    state->target = 0;
    state->last_cursor = view_compute_cursor(app, state->view, seek_pos(pos));
    state->mode = goto_mode_abs;
    
    global_one_time_message = string_u8_litexpr("Goto");
    global_show_one_time_message = true;
}

function void modal_bind_mode_goto(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(mode_goto);
    ParentMap(mapid_vital);
    
    Bind(goto_chord_seek_beginning_of_line,         KeyCode_H);
    Bind(goto_chord_seek_end_of_line,               KeyCode_L);
    Bind(goto_chord_seek_beginning_of_textual_line, KeyCode_H, KeyCode_Shift);
    Bind(goto_chord_seek_end_of_textual_line,       KeyCode_L, KeyCode_Shift);
    
    Bind(goto_chord_seek_beginning_of_file, KeyCode_K);
    Bind(goto_chord_seek_beginning_of_file, KeyCode_G);
    Bind(goto_chord_seek_end_of_file,       KeyCode_J);
    Bind(goto_chord_seek_end_of_file,       KeyCode_G, KeyCode_Shift);
    
    Bind(goto_switch_to_abs, KeyCode_Equal);
    Bind(goto_switch_to_add, KeyCode_Equal, KeyCode_Shift);
    Bind(goto_switch_to_sub, KeyCode_Minus);
    
    Bind(modal_set_mode_normal, KeyCode_Return);
    Bind(goto_cancel, KeyCode_Escape);
    
    Bind(goto_backspace, KeyCode_Backspace);
    BindTextInput(goto_handle_input);
}

CUSTOM_COMMAND_SIG(modal_set_theme_mode_goto)
CUSTOM_DOC("Sets the theme for 'goto' mode.") {
    MODAL_THEME_SET_COLOR(margin,        0, 0xff034449);
    MODAL_THEME_SET_COLOR(margin_hover,  0, 0xff021f4f);
    MODAL_THEME_SET_COLOR(margin_active, 0, 0xff055459);
    
    MODAL_THEME_SET_COLOR(cursor, 0, 0xff207469);
    MODAL_THEME_SET_COLOR(mark,   0, 0xff0d6695);
}

CUSTOM_COMMAND_SIG(goto_chord_seek_beginning_of_line)
CUSTOM_DOC("Places the cursor at the beginning of the line."){
    seek_beginning_of_line(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(goto_chord_seek_beginning_of_textual_line)
CUSTOM_DOC("Places the cursor at the beginning of the textual line."){
    seek_beginning_of_textual_line(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(goto_chord_seek_end_of_line)
CUSTOM_DOC("Places the cursor at the end of the line."){
    seek_end_of_line(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(goto_chord_seek_end_of_textual_line)
CUSTOM_DOC("Places the cursor at the end of the textual line."){
    seek_end_of_textual_line(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(goto_chord_seek_beginning_of_file)
CUSTOM_DOC("Places the cursor at the beginning of the file."){
    goto_beginning_of_file(app);
    modal_revert_mode(app);
}

CUSTOM_COMMAND_SIG(goto_chord_seek_end_of_file)
CUSTOM_DOC("Places the cursor at the end of the file."){
    goto_end_of_file(app);
    modal_revert_mode(app);
}

function void goto_update(Application_Links *app) {
    Scratch_Block scratch(app);
    Goto_State *state = &global_goto_state;
    
    i64 target_line;
    switch(state->mode){
        case goto_mode_abs:{
            target_line = state->target;
        } break;
        case goto_mode_add:{
            target_line = state->last_cursor.line + state->target;
        } break;
        case goto_mode_sub:{
            target_line = state->last_cursor.line - state->target;
        } break;
        default:
        case goto_mode_cancel:{
            target_line = state->last_cursor.line;
        } break;
    }
    view_set_cursor(app, state->view, seek_line_col(target_line, state->last_cursor.col));
    if(target_line < 1) target_line = 1;
}

CUSTOM_COMMAND_SIG(goto_handle_input)
CUSTOM_DOC("Handles input for the goto mode."){
    Goto_State *state = &global_goto_state;
    
    User_Input in = get_current_input(app);
    String_Const_u8 insert = to_writable(&in);
    if(!insert.str || insert.size == 0) return;
    
    String_Const_char str = SCchar(insert);
    char c = str.str[0];
    if(character_is_base10(c)) {
        state->target *= 10;
        state->target += (c - '0');
    }
    else if (c == '+') state->mode = goto_mode_add;
    else if (c == '-') state->mode = goto_mode_sub;
    else if (c == '=') state->mode = goto_mode_abs;
    
    goto_update(app);
}

CUSTOM_COMMAND_SIG(goto_switch_to_add)
CUSTOM_DOC("Sets the goto chord mode to addition, then calculates the target line number."){
    Goto_State *state = &global_goto_state;
    state->mode = goto_mode_add;
    goto_update(app);
}

CUSTOM_COMMAND_SIG(goto_switch_to_sub)
CUSTOM_DOC("Sets the goto chord mode to subtraction, then calculates the target line number."){
    Goto_State *state = &global_goto_state;
    state->mode = goto_mode_sub;
    goto_update(app);
}

CUSTOM_COMMAND_SIG(goto_switch_to_abs)
CUSTOM_DOC("Sets the goto chord mode to absolute, then calculates the target line number."){
    Goto_State *state = &global_goto_state;
    state->mode = goto_mode_abs;
    goto_update(app);
}

CUSTOM_COMMAND_SIG(goto_backspace)
CUSTOM_DOC("Devides the goto line number input by 10, then calculates the target line number."){
    Goto_State *state = &global_goto_state;
    state->target /= 10;
    goto_update(app);
}

CUSTOM_COMMAND_SIG(goto_cancel)
CUSTOM_DOC("Seek back to the line number before entering chord goto mode."){
    Goto_State *state = &global_goto_state;
    state->mode = goto_mode_cancel;
    goto_update(app);
    modal_revert_mode(app);
}

#endif // _MOD_GOTO_CPP
