#if !defined(_MOD_QUERY_CPP)
#define _MOD_QUERY_CPP

//
// Mode Query
//

CUSTOM_COMMAND_SIG(query_next_stage) {
    Query_State *query = active_query_state;
    query->stage += 1;
    query->stage %= MOD_QUERY_STAGE_CAP;
    query->paste_index = 0;
    query->completion_pos = -1;
    query->selection.min = 0;
    query->selection.max = 0;
    query->string[query->stage].size = 0;
    query->applied = false;
}

CUSTOM_COMMAND_SIG(modal_set_mode_query)
CUSTOM_DOC("Activates 'query' mode.") {
    query_start(app, &global_general_query, mode_query, false);
}

CUSTOM_COMMAND_SIG(query_start)
CUSTOM_DOC("Activates 'query' mode.") {
    query_start(app, &global_general_query, mode_query, false);
}

CUSTOM_COMMAND_SIG(query_start_on_token)
CUSTOM_DOC("Activates 'query' mode on the token under the active cursor.") {
    query_start(app, &global_general_query, mode_query, true);
}

function void modal_bind_mode_query(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(mode_query);
    ParentMap(mapid_vital);
    
    Bind(query_delete_char_backward, KeyCode_Backspace);
    Bind(query_delete_word_backward, KeyCode_Backspace, KeyCode_Control);
    Bind(query_delete_word_backward, KeyCode_W,         KeyCode_Control);
    
    Bind(query_delete_char_forward, KeyCode_Delete);
    Bind(query_delete_word_forward, KeyCode_Delete, KeyCode_Control);
    
    Bind(query_move_cursor_left,  KeyCode_Left);
    Bind(query_move_cursor_right, KeyCode_Right);
    
    Bind(query_grow_selection_left,  KeyCode_Left,  KeyCode_Shift);
    Bind(query_grow_selection_right, KeyCode_Right, KeyCode_Shift);
    
    Bind(query_move_cursor_word_left,  KeyCode_Left,  KeyCode_Control);
    Bind(query_move_cursor_word_right, KeyCode_Right, KeyCode_Control);
    
    Bind(query_grow_selection_word_left,  KeyCode_Left,  KeyCode_Control, KeyCode_Shift);
    Bind(query_grow_selection_word_right, KeyCode_Right, KeyCode_Control, KeyCode_Shift);
    
    Bind(query_move_cursor_home, KeyCode_Home);
    Bind(query_move_cursor_end,  KeyCode_End);
    
    Bind(query_grow_selection_home, KeyCode_Home, KeyCode_Shift);
    Bind(query_grow_selection_end,  KeyCode_End,  KeyCode_Shift);
    
    Bind(query_select_all, KeyCode_A,  KeyCode_Control);
    
    Bind(query_history_prev, KeyCode_Up);
    Bind(query_history_next, KeyCode_Down);
    
    Bind(query_paste_next, KeyCode_V, KeyCode_Control);
    Bind(query_copy,       KeyCode_C, KeyCode_Control);
    Bind(query_cut,        KeyCode_X, KeyCode_Control);
    
    Bind(query_toggle_case_sensitive, KeyCode_C, KeyCode_Alt);
    
    Bind(query_insert_tab,     KeyCode_Tab,    KeyCode_Shift);
    Bind(query_insert_newline, KeyCode_Return, KeyCode_Shift);
    
    Bind(query_apply,  KeyCode_Return);
    Bind(query_cancel, KeyCode_Escape);
    
    BindTextInput(query_handle_input);
}

CUSTOM_COMMAND_SIG(query_clear)
CUSTOM_DOC("Clears the input of the active query."){
    Query_State *query = active_query_state;
    query->string[query->stage].size = 0;
    query->selection.min = 0;
    query->selection.max = 0;
    query->completion_pos = -1;
    query->paste_index = 0;
    if(query->update_hook) query->update_hook(app);
}

function void query_delete_selection(Query_State *query) {
    String_u8 *string = &query->string[query->stage];
    Range_i64 post_cursor_range = Ii64(query->selection.max, string->size);
    String_Const_u8 copy_string = string_substring(SCu8(*string), post_cursor_range);
    block_copy(string->str + query->selection.min, copy_string.str, range_size(post_cursor_range));
    string->size -= range_size(query->selection);
    query->selection.max = query->selection.min;
}

CUSTOM_COMMAND_SIG(query_delete_char_backward)
CUSTOM_DOC("Deletes the character to the left of the query cursor."){
    Query_State *query = active_query_state;
    
    if(query->selection.min < query->selection.max) {
        query_delete_selection(query);
    } else {
        if(query->selection.min > 0) {
            query->selection.min -= 1;
            query_delete_selection(query);
            
            query->completion_pos = -1;
            query->paste_index = 0;
        }
    }
    
    if(query->update_hook) query->update_hook(app);
}

CUSTOM_COMMAND_SIG(query_delete_char_forward)
CUSTOM_DOC("Deletes the character to the right of the query cursor."){
    Query_State *query = active_query_state;
    
    if(query->selection.min < query->selection.max) {
        query_delete_selection(query);
    } else {
        String_u8 *string = &query->string[query->stage];
        if(query->selection.min < (i64)string->size) {
            query->selection.max += 1;
            query_delete_selection(query);
            
            query->completion_pos = -1;
            query->paste_index = 0;
        }
    }
    
    if(query->update_hook) query->update_hook(app);
}

CUSTOM_COMMAND_SIG(query_delete_word_backward)
CUSTOM_DOC("Deletes to the start of word left to the query cursor."){
    Query_State *query = active_query_state;
    
    if(query->selection.min < query->selection.max) {
        query_delete_selection(query);
    } else {
        String_u8 *string = &query->string[query->stage];
        if(query->selection.min > 0) {
            query->selection.min = get_word_start(SCu8(*string), query->selection.min);
            query_delete_selection(query);
            
            query->completion_pos = -1;
            query->paste_index = 0;
        }
    }
    
    if(query->update_hook) query->update_hook(app);
}

CUSTOM_COMMAND_SIG(query_delete_word_forward)
CUSTOM_DOC("Deletes to the start of word right to the query cursor."){
    Query_State *query = active_query_state;
    
    if(query->selection.min < query->selection.max) {
        query_delete_selection(query);
    } else {
        String_u8 *string = &query->string[query->stage];
        if(query->selection.min < (i64)string->size) {
            query->selection.max = get_next_word_start(SCu8(*string), query->selection.min);
            query_delete_selection(query);
            
            query->completion_pos = -1;
            query->paste_index = 0;
        }
    }
    
    if(query->update_hook) query->update_hook(app);
}

CUSTOM_COMMAND_SIG(query_history_prev)
CUSTOM_DOC("Moves back in the query history."){
    Query_State *query = active_query_state;
    if(!query->history->prev->string.str) return;
    query->history = query->history->prev;
    query->string[query->stage].size = query->history->string.size;
    query->selection.min = query->history->string.size;
    query->selection.max = query->history->string.size;
    block_copy(query->string[query->stage].str, query->history->string.str, query->string[query->stage].size);
    
    if(query->update_hook) query->update_hook(app);
}

CUSTOM_COMMAND_SIG(query_history_next)
CUSTOM_DOC("Moves forward in the query history."){
    Query_State *query = active_query_state;
    if(!query->history->next->string.str) return;
    query->history = query->history->next;
    query->string[query->stage].size = query->history->string.size;
    query->selection.min = query->history->string.size;
    query->selection.max = query->history->string.size;
    block_copy(query->string[query->stage].str, query->history->string.str, query->string[query->stage].size);
    
    if(query->update_hook) query->update_hook(app);
}

CUSTOM_COMMAND_SIG(query_copy)
CUSTOM_DOC("Like copy, but from the query query."){
    Query_State *query = active_query_state;
    String_Const_u8 selection = string_substring(SCu8(query->string[query->stage]), query->selection);
    clipboard_post(app, 0, selection);
}

CUSTOM_COMMAND_SIG(query_cut)
CUSTOM_DOC("Like cut, but from the query query."){
    Query_State *query = active_query_state;
    query_copy(app);
    query_delete_selection(query);
    query->completion_pos = -1;
    query->paste_index = 0;
    
    if(query->update_hook) query->update_hook(app);
}

function void query_insert_string(Query_State *query, String_Const_u8 insert) {
    String_u8 *string = &query->string[query->stage];
    if(string->size + insert.size >= string->cap) return;
    
    if((i64)string->size > query->selection.min) {
        Range_i64 suffix_range = Ii64(query->selection.min, string->size);
        String_Const_u8 suffix = string_substring(SCu8(*string), suffix_range);
        block_copy(string->str + query->selection.min + insert.size, suffix.str, suffix.size);
        block_copy(string->str + query->selection.min, insert.str, insert.size);
        string->size += insert.size;
    } else {
        string_append(string, insert);
    }
    
    query->selection.min += insert.size;
    query->selection.max = query->selection.min;
}

CUSTOM_COMMAND_SIG(query_paste)
CUSTOM_DOC("Like paste, but in the query query."){
    Scratch_Block scratch(app);
    Query_State *query = active_query_state;
    
    clipboard_update_history_from_system(app, 0);
    i32 count = clipboard_count(0);
    if (count > 0){
        if(query->selection.min < query->selection.max) {
            query_delete_selection(query);
        }
        
        String_Const_u8 string = push_clipboard_index(scratch, 0, 0);
        query->paste_index = 1;
        
        if(string.size > 0) {
            query->pre_paste_cursor = query->selection.min;
            query_insert_string(query, string);
            query->selection.min = query->pre_paste_cursor + string.size;
            query->selection.max = query->pre_paste_cursor + string.size;
        }
        
        if(query->update_hook) query->update_hook(app);
    }
}

CUSTOM_COMMAND_SIG(query_paste_next)
CUSTOM_DOC("Like paste_next, but in the query query."){
    Scratch_Block scratch(app);
    Query_State *query = active_query_state;
    
    i32 count = clipboard_count(0);
    
    if(count == 0 || query->paste_index == 0) {
        query_paste(app);
    } else {
        if(query->paste_index >= count) {
            query->selection.min = query->pre_paste_cursor;
            query_delete_selection(query);
            query->paste_index = 0;
        } 
        else {
            String_Const_u8 string = push_clipboard_index(scratch, 0, query->paste_index);
            query->paste_index += 1;
            
            query->selection.min = query->pre_paste_cursor;
            query_delete_selection(query);
            if(string.size > 0) {
                query_insert_string(query, string);
                query->selection.min = query->pre_paste_cursor + string.size;
                query->selection.max = query->pre_paste_cursor + string.size;
            }
        }
        
        if(query->update_hook) query->update_hook(app);
    }
}

CUSTOM_COMMAND_SIG(query_insert_tab)
CUSTOM_DOC("Insert a newline symbol to the query."){
    Query_State *query = active_query_state;
    
    if(query->selection.min < query->selection.max) {
        query_delete_selection(query);
    }
    
    query_insert_string(query, string_u8_litexpr("\t"));
    
    if(query->update_hook) query->update_hook(app);
}

CUSTOM_COMMAND_SIG(query_insert_newline)
CUSTOM_DOC("Insert a newline symbol to the query."){
    Query_State *query = active_query_state;
    
    if(query->selection.min < query->selection.max) {
        query_delete_selection(query);
    }
    
    View_ID view = get_active_view(app, Access_Always);
    Buffer_ID buffer = view_get_buffer(app, view, Access_Always);
    Managed_Scope scope = buffer_get_managed_scope(app, buffer);
    Line_Ending_Kind *eol_setting = scope_attachment(app, scope, buffer_eol_setting, Line_Ending_Kind);
    
    if(eol_setting && *eol_setting == LineEndingKind_CRLF) {
        query_insert_string(query, string_u8_litexpr("\r\n"));
    } else {
        query_insert_string(query, string_u8_litexpr("\n"));
    }
    
    if(query->update_hook) query->update_hook(app);
}

CUSTOM_COMMAND_SIG(query_handle_input)
CUSTOM_DOC("Handles general input for the query mode."){
    Query_State *query = active_query_state;
    
    query->completion_pos = -1;
    query->paste_index = 0;
    
    User_Input in = get_current_input(app);
    
    String_Const_u8 insert = to_writable(&in);
    if(insert.str && insert.size > 0) {
        if(query->selection.min < query->selection.max) {
            query_delete_selection(query);
        }
        
        query_insert_string(query, insert);
    }
    
    if(query->update_hook) query->update_hook(app);
}

CUSTOM_COMMAND_SIG(query_move_cursor_left)
CUSTOM_DOC("Moves the input cursor one character to the left."){
    Query_State *query = active_query_state;
    
    query_grow_selection_left(app);
    query->selection.max = query->selection.min;
}

CUSTOM_COMMAND_SIG(query_move_cursor_right)
CUSTOM_DOC("Moves the input cursor one character to the right."){
    Query_State *query = active_query_state;
    
    query_grow_selection_right(app);
    query->selection.min = query->selection.max;
}

CUSTOM_COMMAND_SIG(query_grow_selection_left)
CUSTOM_DOC("Grows the selection another character to the left."){
    Query_State *query = active_query_state;
    
    query->completion_pos = -1;
    query->paste_index = 0;
    query->selection.min = clamp_bot(query->selection.min - 1, 0);
}

CUSTOM_COMMAND_SIG(query_grow_selection_right)
CUSTOM_DOC("Grows the selection another character to the right."){
    Query_State *query = active_query_state;
    
    query->completion_pos = -1;
    query->paste_index = 0;
    query->selection.max = clamp_top(query->selection.max + 1,
                                     (i64)query->string[query->stage].size);
}

CUSTOM_COMMAND_SIG(query_move_cursor_word_left)
CUSTOM_DOC("Moves the input cursor one character to the left."){
    Query_State *query = active_query_state;
    
    query_grow_selection_word_left(app);
    query->selection.max = query->selection.min;
}

CUSTOM_COMMAND_SIG(query_move_cursor_word_right)
CUSTOM_DOC("Moves the input cursor one character to the right."){
    Query_State *query = active_query_state;
    
    query_grow_selection_word_right(app);
    query->selection.min = query->selection.max;
}

CUSTOM_COMMAND_SIG(query_grow_selection_word_left)
CUSTOM_DOC("Grows the selection another character to the left."){
    Query_State *query = active_query_state;
    
    query->completion_pos = -1;
    query->paste_index = 0;
    String_u8 *string = &query->string[query->stage];
    i64 pos = get_word_start(SCu8(*string), query->selection.min);
    query->selection.min = clamp_bot(pos, 0);
}

CUSTOM_COMMAND_SIG(query_grow_selection_word_right)
CUSTOM_DOC("Grows the selection another character to the right."){
    Query_State *query = active_query_state;
    
    query->completion_pos = -1;
    query->paste_index = 0;
    String_u8 *string = &query->string[query->stage];
    i64 pos = get_next_word_start(SCu8(*string), query->selection.max);
    query->selection.max = clamp_top(pos,
                                     (i64)query->string[query->stage].size);
}

CUSTOM_COMMAND_SIG(query_move_cursor_home)
CUSTOM_DOC("Moves the input cursor to the begining of the query string."){
    Query_State *query = active_query_state;
    
    query_grow_selection_home(app);
    query->selection.max = query->selection.min;
}

CUSTOM_COMMAND_SIG(query_move_cursor_end)
CUSTOM_DOC("Moves the input cursor to the end of the query string."){
    Query_State *query = active_query_state;
    
    query_grow_selection_end(app);
    query->selection.min = query->selection.max;
}

CUSTOM_COMMAND_SIG(query_grow_selection_home)
CUSTOM_DOC("Grows the selection to the begining of the query string."){
    Query_State *query = active_query_state;
    
    query->completion_pos = -1;
    query->paste_index = 0;
    query->selection.min = 0;
}

CUSTOM_COMMAND_SIG(query_grow_selection_end)
CUSTOM_DOC("Grows the selection to the end of the query string."){
    Query_State *query = active_query_state;
    
    query->completion_pos = -1;
    query->paste_index = 0;
    query->selection.max = query->string[query->stage].size;
}

CUSTOM_COMMAND_SIG(query_select_all)
CUSTOM_DOC("Selects all of the query string."){
    Query_State *query = active_query_state;
    
    query->completion_pos = -1;
    query->paste_index = 0;
    query->selection.min = 0;
    query->selection.max = query->string[query->stage].size;
}

function String_Const_u8
buffer_get_word_completion(Application_Links *app, Buffer_ID buffer, b32 first_completion, Range_i64 needle_range, i64 pos){
    String_Const_u8 str = {};
    if(buffer_exists(app, buffer)) {
        Word_Complete_Iterator *it = word_complete_get_shared_iter(app);
        local_persist b32 initialized = false;
        
        if(first_completion || !initialized) {
            initialized = false;
            if(range_size(needle_range) > 0) {
                initialized = true;
                word_complete_iter_init(buffer, needle_range, it);
                word_complete_iter_stop_on_this_buffer(it);
            }
        }
        
        if(initialized) {
            word_complete_iter_next(it);
            str = word_complete_iter_read(it);
        }
    }
    
    return str;
}

CUSTOM_COMMAND_SIG(query_toggle_case_sensitive)
CUSTOM_DOC("Toggles query mode case-sensitivity."){
    Query_State *query = active_query_state;
    query->match_flags ^= StringMatch_CaseSensitive;
}

CUSTOM_COMMAND_SIG(query_cancel)
CUSTOM_DOC("Finish the query and call to the query_cancel hook."){
    Query_State *query = active_query_state;
    modal_revert_mode(app);
    
    if(query->cancel_hook) {
        query->cancel_hook(app);
    }
}

CUSTOM_COMMAND_SIG(query_apply)
CUSTOM_DOC("Finish the query and call to the query_apply hook."){
    Query_State *query = active_query_state;
    
    String_u8 string = query->string[query->stage];
    string_null_terminate(&string);
    
    if(string.size > 0) {
        Dll_Node_String_Const_u8 *node = query->history_first->next;
        b32 new_key = true;
        while(node != query->history_first) {
            if(string_compare(node->string, SCu8(string)) == 0) {
                new_key = false;
                dll_remove(node);
                break;
            }
            node = node->next;
        }
        if(new_key) {
            node = push_array(&query->arena, Dll_Node_String_Const_u8, 1);
            node->string = push_string_copy(&query->arena, SCu8(string));
        }
        
        dll_insert_back(query->history_first, node);
        query->history = query->history_first;
    }
    
    query->applied = true;
    if(query->apply_hook) {
        query->apply_hook(app);
    }
    if(query->applied) {
        modal_revert_mode(app);
    }
}


#endif // _MOD_QUERY_CPP