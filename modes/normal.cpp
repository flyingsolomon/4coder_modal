#if !defined(_MOD_NORMAL_CPP)
#define _MOD_NORMAL_CPP

//
// Mode Normal
//
CUSTOM_COMMAND_SIG(modal_set_mode_normal)
CUSTOM_DOC("activates 'normal' mode.") {
    modal_set_active_mapid(app, mode_normal);
}

function void modal_bind_mode_normal(Mapping *mapping) {
    MappingScope();
    SelectMapping(mapping);
    
    SelectMap(mode_normal);
    ParentMap(mapid_common);
    
    // ---- Modes/Chords ----
    // Insert {
    Bind(modal_set_mode_insert,            KeyCode_I);
    Bind(modal_set_chord_insert,           KeyCode_I, KeyCode_Shift);
    Bind(modal_seek_eol_then_insert,       KeyCode_A, KeyCode_Shift);
    Bind(modal_newline_after_then_insert,  KeyCode_O);
    Bind(modal_newline_before_then_insert, KeyCode_O, KeyCode_Shift);
    // }
    
    Bind(modal_set_mode_goto, KeyCode_G); // Goto
    Bind(goto_end_of_file,    KeyCode_G,  KeyCode_Shift);
    Bind(modal_set_mode_preferences, KeyCode_P); // Preferences
    
    // Replace {
    Bind(modal_set_chord_replace, KeyCode_R);
    Bind(modal_set_mode_replace,  KeyCode_R, KeyCode_Shift);
    Bind(modal_set_mode_replace,  KeyCode_Insert);
    // }
    
    // Search {
    Bind(modal_set_mode_search,          KeyCode_ForwardSlash);
    Bind(modal_set_mode_search_on_token, KeyCode_ForwardSlash, KeyCode_Shift);
    Bind(search_disable_highlights,      KeyCode_Escape);
    // }
    
    Bind(modal_set_chord_snippet, KeyCode_3);
    Bind(modal_set_chord_case,    KeyCode_4);
    
    // ---- Debugger ----
    Bind(debugger_jump_to_cursor,              KeyCode_Return, KeyCode_Alt);
    Bind(debugger_add_breakpoint_at_cursor,    KeyCode_Return, KeyCode_Control);
    Bind(debugger_remove_breakpoint_at_cursor, KeyCode_Return, KeyCode_Control, KeyCode_Shift);
    
    // ---- Navigating ----
    Bind(move_left,  KeyCode_H);
    Bind(move_right, KeyCode_L);
    Bind(move_up,    KeyCode_K);
    Bind(move_down,  KeyCode_J);
    
    Bind(seek_end_of_textual_line,       KeyCode_9);
    Bind(seek_end_of_line,               KeyCode_9, KeyCode_Control);
    Bind(seek_beginning_of_textual_line, KeyCode_0);
    Bind(seek_beginning_of_line,         KeyCode_0, KeyCode_Control);
    
    Bind(cursor_mark_swap, KeyCode_Semicolon);
    Bind(center_view, KeyCode_A);
    
    Bind(change_active_panel,           KeyCode_W);
    Bind(change_active_panel_backwards, KeyCode_W, KeyCode_Shift);
    
    Bind(change_to_build_panel, KeyCode_Equal);
    Bind(close_build_panel,     KeyCode_Minus);
    
    Bind(search_find_or_jump_next, KeyCode_N);
    Bind(search_find_or_jump_prev, KeyCode_N, KeyCode_Shift);
    Bind(goto_first_jump, KeyCode_M);
    
    Bind(if_read_only_goto_position_same_panel, KeyCode_Return, KeyCode_Shift);
    
    Bind(interactive_switch_buffer, KeyCode_B);
    Bind(interactive_open_or_new,   KeyCode_F);
    Bind(open_in_other,             KeyCode_F, KeyCode_Shift);
    
    Bind(open_file_in_quotes,               KeyCode_1);
    Bind(mod_open_matching_file_same_panel, KeyCode_2);
    Bind(mod_open_matching_file,            KeyCode_2, KeyCode_Shift);
    
    // ---- Editing ----
    Bind(delete_char, KeyCode_X);
    
    Bind(undo, KeyCode_Z);
    Bind(redo, KeyCode_Z, KeyCode_Shift);
    Bind(undo, KeyCode_U);
    Bind(redo, KeyCode_U, KeyCode_Shift);
    
    Bind(duplicate_line,     KeyCode_D);
    Bind(delete_line,        KeyCode_D, KeyCode_Shift);
    Bind(copy,               KeyCode_C);
    Bind(mod_select_line,    KeyCode_L, KeyCode_Shift);
    Bind(mod_copy_line,      KeyCode_C, KeyCode_Shift);
    Bind(mod_cut_line,       KeyCode_X, KeyCode_Shift);
    Bind(paste,              KeyCode_V);
    Bind(paste_next,         KeyCode_V, KeyCode_Shift);
    Bind(mod_backspace_line, KeyCode_Backspace, KeyCode_Shift);
    
    Bind(save,               KeyCode_S);
    Bind(kill_buffer,        KeyCode_K, KeyCode_Shift);
    Bind(mod_toggle_scratch, KeyCode_B, KeyCode_Shift);
    Bind(mod_toggle_search,  KeyCode_S, KeyCode_Shift);
    // Bind(mod_open_messages_buffer,     KeyCode_M, KeyCode_Shift);
    
    Bind(set_mark,    KeyCode_Space);
    Bind(mod_toggler, KeyCode_Period);
    
    Bind(mod_increase_indent, KeyCode_Period, KeyCode_Shift);
    Bind(mod_decrease_indent, KeyCode_Comma,  KeyCode_Shift);
    
    Bind(mod_range_increase_indent, KeyCode_Period, KeyCode_Shift, KeyCode_Control);
    Bind(mod_range_decrease_indent, KeyCode_Comma,  KeyCode_Shift, KeyCode_Control);
    
    Bind(mod_write_underscore, KeyCode_Minus, KeyCode_Shift);
    Bind(mod_write_space,      KeyCode_Space, KeyCode_Shift);
    
    Bind(search_list_all_substring_locations, KeyCode_T);
    Bind(list_all_locations_of_identifier, KeyCode_T, KeyCode_Shift);
    
    Bind(mod_keyboard_macro_start_recording, KeyCode_Q);
    Bind(mod_keyboard_macro_replay,          KeyCode_Tick, KeyCode_Shift);
    Bind(mod_keyboard_macro_replay_last,     KeyCode_Tick);
    
    Bind(command_lister,           KeyCode_Semicolon, KeyCode_Shift);
    Bind(mod_execute_any_cli,      KeyCode_1,         KeyCode_Shift);
    Bind(mod_execute_previous_cli, KeyCode_1,         KeyCode_Control, KeyCode_Shift);
    
    Bind(select_surrounding_scope,   KeyCode_LeftBracket);
    Bind(select_prev_scope_absolute, KeyCode_RightBracket);
    Bind(select_next_scope_absolute, KeyCode_Quote);
    Bind(place_in_scope,             KeyCode_LeftBracket, KeyCode_Shift);
    // Bind(scope_absorb_down,          KeyCode_RightBracket, KeyCode_Shift);
    
    Bind(mod_comment_range,      KeyCode_8, KeyCode_Shift);
    Bind(mod_parenthesize_range, KeyCode_9, KeyCode_Shift);
    Bind(mod_quote_range,        KeyCode_Quote, KeyCode_Shift);
    
    Bind(mod_decrement_digit_decimal,           KeyCode_5);
    Bind(mod_decrement_token_decimal,           KeyCode_5, KeyCode_Shift);
    Bind(mod_increment_digit_decimal,           KeyCode_6);
    Bind(mod_increment_token_decimal,           KeyCode_6, KeyCode_Shift);
    
    Bind(mod_include_gaurd, KeyCode_3, KeyCode_Shift);
    
    BindTextInput(normal_handle_input);
    
    
}

CUSTOM_COMMAND_SIG(modal_set_theme_mode_normal)
CUSTOM_DOC("Sets the theme for 'normal' mode.") {
    // Same as the base theme, nothing to do.
}

CUSTOM_COMMAND_SIG(normal_handle_input)
CUSTOM_DOC("Handles input for the normal mode."){
    User_Input in = get_current_input(app);
    
    String_Const_u8 insert = to_writable(&in);
    if(insert.str && insert.size > 0 && character_is_whitespace(insert.str[0])) {
        write_text(app, insert);
    }
}

#endif // _MOD_NORMAL_CPP
