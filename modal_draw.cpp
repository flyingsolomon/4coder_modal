#if !defined(_MODAL_DRAW_CPP)
#define _MODAL_DRAW_CPP

function u64 string_find_first_whitespace(String_Const_u8 str, u64 start_pos) {
    u64 i = start_pos;
    for (;i < str.size && !character_is_whitespace(str.str[i]); i += 1);
    return(i);
}

function void mod_push_fancy_string_whitespace_formatted(Arena *arena, Fancy_Line *list, FColor base_color, FColor whitespace_color, String_Const_u8 str) {
    u64 old_pos = 0;
    u64 pos = string_find_first_whitespace(str, 0);
    while (pos < str.size) {
        String_Const_u8 push_str = string_substring(str, Ii64(old_pos, pos));
        push_fancy_string(arena, list, base_color, push_str);
        String_Const_u8 whitespace_string;
        switch(str.str[pos]) {
            case '\n': { whitespace_string = string_u8_litexpr("\\n"); } break;
            case '\r': { whitespace_string = string_u8_litexpr("\\r"); } break;
            case '\t': { whitespace_string = string_u8_litexpr("\\t"); } break;
            case '\f': { whitespace_string = string_u8_litexpr("\\f"); } break;
            case '\v': { whitespace_string = string_u8_litexpr("\\v"); } break;
            case ' ':  { whitespace_string = string_u8_litexpr(" ");   } break;
            default:   { whitespace_string = string_u8_litexpr("\\?"); } break;
        }
        push_fancy_string(arena, list, whitespace_color, whitespace_string);
        old_pos = pos + 1;
        pos = string_find_first_whitespace(str, old_pos);
    }
    String_Const_u8 push_str = string_substring(str, Ii64(old_pos, str.size));
    push_fancy_string(arena, list, base_color, push_str);
}

function void
mod_draw_file_bar(Application_Links *app, View_ID view_id, Buffer_ID buffer, Face_ID face_id, Rect_f32 bar, b32 is_active_view){
    Scratch_Block scratch(app);
    
    FColor bar_color  = fcolor_id(defcolor_bar);
    FColor base_color = fcolor_id(defcolor_base);
    FColor mode_color = fcolor_change_alpha(fcolor_id(defcolor_cursor), 1);
    FColor back_color = fcolor_id(defcolor_back);
    FColor pop1_color = fcolor_id(defcolor_pop1);
    FColor pop2_color = fcolor_id(defcolor_pop2);
    
    draw_rectangle_fcolor(app, bar, 0.f, bar_color);
    Rect_f32 right_rect = bar;
    Rect_f32 left_rect  = bar;
    
    Fancy_Line right_list = {};
    Fancy_Line left_list  = {};
    
    { // Right-Aligned
        push_fancy_stringf(scratch, &right_list, fcolor_change_alpha(bar_color, 1), "|");
        
        if(global_keyboard_macro_is_recording) {
            push_fancy_stringf(scratch, &right_list, pop2_color, "•RECORDING [%s] ", key_code_name[global_active_macro_code]);
        }
        
        
        if(is_active_view &&
           (global_active_mapid == (Command_Map_ID)mode_query ||
            global_active_mapid == (Command_Map_ID)mode_search) &&
           (global_search_state.query.match_flags & StringMatch_CaseSensitive)) {
            push_fancy_string(scratch, &right_list, pop1_color, string_u8_litexpr("Case Sensitive "));
        }
        
        i64 cursor_position = view_get_cursor_pos(app, view_id);
        Buffer_Cursor cursor = view_compute_cursor(app, view_id, seek_pos(cursor_position));
        push_fancy_stringf(scratch, &right_list, base_color, "L%lld,C%lld", cursor.line, cursor.col);
        
#if 0
        if(is_active_view) {
            String_Const_u8 mode_name = modal_get_mode_name(global_active_mapid);
            if(mode_name.size > 8) mode_name.size = 8;
            
            u64 pad_size = (8 - mode_name.size)/2;
            u8 pad[8];
            block_fill_u8(pad, 8, ' ');
            
            f32 right_string_width = get_fancy_line_width(app, face_id, &right_list);
            if(mode_name.size % 2) {
                push_fancy_string(scratch, &right_list, base_color, string_u8_litexpr(" "));
            }
            push_fancy_stringf(scratch, &right_list, back_color, " %*.s%.*s%*.s ", pad_size, pad, string_expand(mode_name), pad_size, pad);
            
            f32 mode_width = get_fancy_line_width(app, face_id, &right_list) - right_string_width;
            Rect_f32 mode_back = {bar.x1 - mode_width, bar.y0, bar.x1, bar.y1 - 2.f};
            draw_rectangle_fcolor(app, mode_back, 5.f, fcolor_blend(mode_color, .2f, base_color));
        }
#endif
        
        f32 width = get_fancy_line_width(app, face_id, &right_list);
        
        right_rect.x0 = bar.x1 - (width + 4.f);
        left_rect.x1 = right_rect.x0;
    }
    
    { // Left-Aligned
        if(is_active_view && global_show_one_time_message) {
            push_fancy_string(scratch, &left_list, pop1_color, global_one_time_message);
        } else {
            if(is_active_view && modal_active_mode_inherits_from(app, mode_query)) {
                Query_State *query = active_query_state;
                String_Const_u8 prompt = query->prompt[query->stage];
                String_Const_u8 string = SCu8(query->string[query->stage]);
                
                Range_i64 pre_cursor_range = Ii64(0, query->selection.min);
                String_Const_u8 pre_cursor = string_substring(string, pre_cursor_range);
                
                String_Const_u8 selection = string_substring(string, query->selection);
                
                Range_i64 post_cursor_range = Ii64(query->selection.max, string.size);
                String_Const_u8 post_cursor = string_substring(string, post_cursor_range);
                
                FColor whitespace_color = fcolor_change_alpha(fcolor_blend(base_color, .33f, pop2_color), .5f);
                push_fancy_string(scratch, &left_list, pop1_color, prompt);
                f32 query_start = get_fancy_line_width(app, face_id, &left_list);
                
                mod_push_fancy_string_whitespace_formatted(scratch, &left_list, base_color, whitespace_color, pre_cursor);
                f32 cursor_start = get_fancy_line_width(app, face_id, &left_list);
                
                mod_push_fancy_string_whitespace_formatted(scratch, &left_list, base_color, whitespace_color, selection);
                f32 cursor_end = get_fancy_line_width(app, face_id, &left_list);
                
                mod_push_fancy_string_whitespace_formatted(scratch, &left_list, base_color, whitespace_color, post_cursor);
                
                f32 query_end = get_fancy_line_width(app, face_id, &left_list);
                Face_Metrics metrics = get_face_metrics(app, face_id);
                if((cursor_end + left_rect.x0) > (left_rect.x1 - metrics.normal_advance*2.f)) {
                    left_rect.x0 -= (cursor_end + left_rect.x0 - left_rect.x1 + metrics.normal_advance*2.f);
                }
                
                {
                    Rect_f32 query_rect;
                    query_rect.y0 = left_rect.y0 + 2.f;
                    query_rect.y1 = left_rect.y1 - 2.f;
                    query_rect.x0 = left_rect.x0 + query_start;
                    query_rect.x1 = left_rect.x0 + query_end + 4.f;
                    
                    f32 roundness = (metrics.normal_advance*0.5f)*0.9f;
                    
                    Rect_f32 cursor_rect;
                    FColor query_cursor_color = mode_color;
                    if(cursor_start == cursor_end) {
                        cursor_rect.y0 = left_rect.y0 + 2.f;
                        cursor_rect.y1 = left_rect.y1 - 2.f;
                        cursor_rect.x0 = left_rect.x0 + cursor_start + 3.f;
                        cursor_rect.x1 = left_rect.x0 + cursor_end   + 5.f;
                    } else {
                        cursor_rect.y0 = left_rect.y0 + 4.f;
                        cursor_rect.y1 = left_rect.y1 - 4.f;
                        cursor_rect.x0 = left_rect.x0 + cursor_start + 2.f;
                        cursor_rect.x1 = left_rect.x0 + cursor_end   + 2.f;
                        query_cursor_color = fcolor_change_alpha(query_cursor_color, .75f);
                    }
                    
                    FColor query_back_color = fcolor_change_alpha(back_color, .75f);
                    draw_rectangle(app, query_rect,  roundness, fcolor_resolve(query_back_color));
                    draw_rectangle(app, cursor_rect, roundness, fcolor_resolve(query_cursor_color));
                }
                
            } else if(is_active_view && (global_active_mapid == (Command_Map_ID)mode_goto)) {
                u8 c = ' ';
                switch(global_goto_state.mode){
                    case goto_mode_add:{ c = '+'; } break;
                    case goto_mode_sub:{ c = '-'; } break;
                }
                
                push_fancy_string(scratch, &left_list, pop1_color, string_u8_litexpr("Goto: "));
                push_fancy_stringf(scratch, &left_list, base_color, "%c%d", c, global_goto_state.target);
            } else {
                String_Const_u8 unique_name = push_buffer_unique_name(app, scratch, buffer);
                push_fancy_string(scratch, &left_list, base_color, unique_name);
                
                Managed_Scope scope = buffer_get_managed_scope(app, buffer);
                Line_Ending_Kind *eol_setting = scope_attachment(app, scope, buffer_eol_setting, Line_Ending_Kind);
                switch (*eol_setting){
                    case LineEndingKind_Binary: { push_fancy_string(scratch, &left_list, base_color, string_u8_litexpr(" - bin")); }  break;
                    case LineEndingKind_LF:     { push_fancy_string(scratch, &left_list, base_color, string_u8_litexpr(" - lf")); }   break;
                    case LineEndingKind_CRLF:   { push_fancy_string(scratch, &left_list, base_color, string_u8_litexpr(" - crlf")); } break;
                }
                
                u8 space[3];
                {
                    Dirty_State dirty = buffer_get_dirty_state(app, buffer);
                    String_u8 str = Su8(space, 0, 3);
                    if (dirty != 0){
                        string_append(&str, string_u8_litexpr(" "));
                    }
                    if (HasFlag(dirty, DirtyState_UnsavedChanges)){
                        string_append(&str, string_u8_litexpr("*"));
                    }
                    if (HasFlag(dirty, DirtyState_UnloadedChanges)){
                        string_append(&str, string_u8_litexpr("!"));
                    }
                    push_fancy_string(scratch, &left_list, pop2_color, str.string);
                }
            }
        }
        
        draw_fancy_line(app, face_id, fcolor_zero(), &left_list, left_rect.p0 + V2f32(2.f, 2.f));
    }
    
    // Draw right side
    {
        draw_rectangle_fcolor(app, right_rect, 3.f, bar_color);
        draw_fancy_line(app, face_id, fcolor_zero(), &right_list, right_rect.p0 + V2f32(0.f, 2.f));
    }
}

/* The following section was stolen shamelessly (and heavily modified) from
   Ryan Fleury's personal 4coder custom layer ; https://github.com/ryanfleury/4coder_fleury/
   Really good stuff. */

function void
fleury4_interpolate_cursor(Application_Links *app, Frame_Info frame_info,
                           Rect_f32 *rect, Rect_f32 *last_rect, Rect_f32 target) {
    *last_rect = *rect;
    
    float x_change = target.x0 - rect->x0;
    float y_change = target.y0 - rect->y0;
    
    float cursor_size_x = (target.x1 - target.x0);
    float cursor_size_y = (target.y1 - target.y0) * (1 + fabsf(y_change) / 60.f);
    
    if (fabs(x_change) > 1.f || fabs(y_change) > 1.f) {
        animate_in_n_milliseconds(app, 0);
    }
    
    rect->x0 += (x_change) * frame_info.animation_dt * 30.f;
    rect->y0 += (y_change) * frame_info.animation_dt * 30.f;
    rect->x1 = rect->x0 + cursor_size_x;
    rect->y1 = rect->y0 + cursor_size_y;
    
    if (target.y0 > last_rect->y0) {
        if (rect->y0 < last_rect->y0) {
            rect->y0 = last_rect->y0;
        }
    } else {
        if (rect->y1 > last_rect->y1) {
            rect->y1 = last_rect->y1;
        }
    }
}

function void
mod_render_cursor(Application_Links *app, View_ID view_id, b32 is_active_view,
                  Buffer_ID buffer, Text_Layout_ID text_layout_id,
                  f32 roundness, f32 outline_thickness, Frame_Info frame_info) {
    Rect_f32 view_rect = view_get_screen_rect(app, view_id);
    Rect_f32 clip = draw_set_clip(app, view_rect);
    Range_i64 visible_range = text_layout_get_visible_range(app, text_layout_id);
    
    draw_highlight_range(app, view_id, buffer, text_layout_id, roundness);
    
    i32 cursor_sub_id = default_cursor_sub_id();
    FColor cursor_color = fcolor_id(defcolor_cursor, cursor_sub_id);
    FColor mark_color   = fcolor_id(defcolor_mark);
    FColor back_color   = fcolor_id(defcolor_back);
    
    i64 cursor_pos = view_get_cursor_pos(app, view_id);
    i64 mark_pos = view_get_mark_pos(app, view_id);
    
    // NOTE(rjf): Draw cursor.
    {
        Rect_f32 cursor_rect;
        f32 cursor_roundness = roundness;
        
        if(is_active_view) {
            Rect_f32 target_cursor = global_cursor_rect;
            if ((cursor_pos >= visible_range.min) && (cursor_pos <= visible_range.max)) {
                target_cursor = text_layout_character_on_screen(app, text_layout_id, cursor_pos);
            } else {
                cursor_color = fcolor_blend(cursor_color, .5f, back_color);
                cursor_roundness *= 3.f;
            }
            
            if(global_smooth_cursor && is_active_view) {
                fleury4_interpolate_cursor(app, frame_info, &global_cursor_rect, &global_last_cursor_rect, target_cursor);
            } else {
                global_last_cursor_rect = target_cursor;
            }
            cursor_rect = global_cursor_rect;
        } else {
            cursor_rect = text_layout_character_on_screen(app, text_layout_id, cursor_pos);
        }
        
        Rect_f32 mark_rect = text_layout_character_on_screen(app, text_layout_id, mark_pos);
        Rect_f32 mark_inner_rect = mark_rect;
        Rect_f32 cursor_inner_rect = cursor_rect;
        
        f32 inner_rect_x_margin = rect_width(mark_rect)/2.f;
        f32 inner_rect_y_margin = rect_height(mark_rect)/3.3f;
        
        if(cursor_pos >= mark_pos) {
            cursor_inner_rect.y1 -= inner_rect_y_margin*2.f;
            if(is_active_view) cursor_inner_rect.x0 -= inner_rect_x_margin/3.f;
            cursor_inner_rect.x1 -= inner_rect_x_margin;
            
            mark_inner_rect.y0 += inner_rect_y_margin*2.f;
            if(is_active_view) mark_inner_rect.x1 += inner_rect_x_margin/3.f;
            mark_inner_rect.x0 += inner_rect_x_margin;
        } else if(cursor_pos < mark_pos) {
            cursor_inner_rect.y0 += inner_rect_y_margin*2.f;
            if(is_active_view) cursor_inner_rect.x1 += inner_rect_x_margin/3.f;
            cursor_inner_rect.x0 += inner_rect_x_margin;
            
            mark_inner_rect.y1 -= inner_rect_y_margin*2.f;
            if(is_active_view) mark_inner_rect.x0 -= inner_rect_x_margin/3.f;
            mark_inner_rect.x1 -= inner_rect_x_margin;
        }
        
        FColor back_cursor_color = fcolor_change_alpha(cursor_color, .6f);
        
        if(is_active_view) {
            draw_rectangle(app, cursor_rect, cursor_roundness, fcolor_resolve(back_cursor_color));
            paint_text_color_pos(app, text_layout_id, cursor_pos, fcolor_id(defcolor_at_cursor));
        } else {
            draw_rectangle_outline(app, cursor_rect, cursor_roundness, outline_thickness*1.5f, fcolor_resolve(cursor_color));
        }
        
        draw_rectangle_outline(app, mark_rect,   cursor_roundness, outline_thickness, fcolor_resolve(mark_color));
        
        draw_rectangle(app, cursor_inner_rect, cursor_roundness, fcolor_resolve(cursor_color));
        draw_rectangle(app, mark_inner_rect,   cursor_roundness, fcolor_resolve(mark_color));
    }
    
    draw_set_clip(app, clip);
}

function Rect_f32_Pair
mod_layout_line_number_margin(Application_Links *app, Buffer_ID buffer, Rect_f32 rect, f32 digit_advance){
    i64 line_count = buffer_get_line_count(app, buffer);
    i64 line_count_digit_count = digit_count_from_integer(line_count*10, 10); // Save a digit for the sign
    return(layout_line_number_margin(rect, digit_advance, line_count_digit_count));
}

function void mod_draw_line_number_margin(Application_Links *app, View_ID view_id, Buffer_ID buffer, Face_ID face_id, Text_Layout_ID text_layout_id, Rect_f32 margin){
    Rect_f32 prev_clip = draw_set_clip(app, margin);
    draw_rectangle_fcolor(app, margin, 0.f, fcolor_id(defcolor_line_numbers_back));
    
    Range_i64 visible_range = text_layout_get_visible_range(app, text_layout_id);
    
    FColor line_color = fcolor_id(defcolor_line_numbers_text);
    
    i64 line_count = buffer_get_line_count(app, buffer);
    i64 line_count_digit_count = digit_count_from_integer(line_count, 10);
    
    Scratch_Block scratch(app);
    
    Buffer_Cursor cursor = view_compute_cursor(app, view_id, seek_pos(visible_range.first));
    i64 line_index = cursor.line;
    
    i64 pos = view_get_cursor_pos(app, view_id);
    i64 line_number = get_line_number_from_pos(app, buffer, pos);
    
    while(line_index < line_number) {
        Range_f32 line_y = text_layout_line_on_screen(app, text_layout_id, line_index);
        Vec2_f32 p = V2f32(margin.x0, line_y.min);
        Temp_Memory_Block temp(scratch);
        Fancy_String *string = push_fancy_stringf(scratch, 0, fcolor_change_alpha(line_color, 0.5f),
                                                  "-%-*lld",
                                                  line_count_digit_count,
                                                  line_number - line_index);
        draw_fancy_string(app, face_id, fcolor_zero(), string, p);
        line_index += 1;
    }
    
    {
        Range_f32 line_y = text_layout_line_on_screen(app, text_layout_id, line_index);
        Vec2_f32 p = V2f32(margin.x0, line_y.min);
        Temp_Memory_Block temp(scratch);
        Fancy_String *string = push_fancy_stringf(scratch, 0, line_color,
                                                  " %*lld",
                                                  line_count_digit_count,
                                                  line_number);
        draw_fancy_string(app, face_id, fcolor_zero(), string, p);
        line_index += 1;
    }
    
    while(line_index <= line_count) {
        Range_f32 line_y = text_layout_line_on_screen(app, text_layout_id, line_index);
        Vec2_f32 p = V2f32(margin.x0, line_y.min);
        Temp_Memory_Block temp(scratch);
        Fancy_String *string = push_fancy_stringf(scratch, 0, fcolor_change_alpha(line_color, 0.5f),
                                                  "+%-*lld",
                                                  line_count_digit_count,
                                                  line_index - line_number);
        draw_fancy_string(app, face_id, fcolor_zero(), string, p);
        line_index += 1;
    }
    
    draw_set_clip(app, prev_clip);
}

function void mod_draw_search_highlights(Application_Links *app, View_ID view_id, Buffer_ID buffer, Text_Layout_ID text_layout_id, f32 roundness) {
    ProfileScope(app, "draw search highlights");
    
    Scratch_Block scratch(app);
    Rect_f32 view_rect = view_get_screen_rect(app, view_id);
    Rect_f32 clip = draw_set_clip(app, view_rect);
    
    Range_i64 visible_range = text_layout_get_visible_range(app, text_layout_id);
    
    String_Match_List matches;
    
    /* NOTE(FS):
       You would think it might be a little  wasteful to iterate the whole visible range every frame,
       but it's actually pretty cheap. It seems that it costs more to push all the highlight rectangles,
       so there's no need to worry about optimizing it out.
    */
    {
        ProfileBlock(app, "draw search highlights buffer find all matches");
        
        String_Const_u8 needle = SCu8(global_search_state.query.string[0]);
        matches = buffer_find_all_matches(app, scratch, buffer, 0, visible_range, needle, &character_predicate_alpha_numeric_underscore_utf8, Scan_Forward);
        String_Match_Flag match_flags = global_search_state.query.match_flags;
        string_match_list_filter_flags(&matches, match_flags, 0);
    }
    
    {
        ProfileBlock(app, "draw search highlights draw matches");
        
        String_Match *match = matches.first;
        while(match) {
            draw_character_block(app, text_layout_id, match->range, roundness, fcolor_id(defcolor_highlight));
            match = match->next;
        }
    }
    
    draw_set_clip(app, clip);
}

#if 0
function void mod_draw_scope_opening_line(Application_Links *app, View_ID view, Buffer_ID buffer, Text_Layout_ID text_layout_id, Face_ID face_id) {
    i64 pos = view_get_cursor_pos(app, view);
    u64 markPos = view_get_mark_pos(app, view);
    if(buffer_get_char(app, buffer, pos) == '}') {
        i64 scope_start = 0;
        if(find_nest_side(app, buffer, pos, FindNest_Scope, Scan_Backward, NestDelim_Open, &scope_start)) {
            Scratch_Block scratch(app);
            
            String_Const_u8 line = push_buffer_line(app, scratch, buffer, seek_pos(scope_start));
            Vec2_f32 p = V2f32();
            draw_fancy_string(app, face_id, fcolor_change_alpha(fcolor_id(defcolor_text_default), .25), line, p);
        }
    }
}
#endif

#endif // _MODAL_DRAW_CPP
