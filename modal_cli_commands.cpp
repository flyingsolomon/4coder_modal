#if !defined(_MODAL_CLI_COMMANDS_CPP)
#define _MODAL_CLI_COMMANDS_CPP

CUSTOM_COMMAND_SIG(mod_execute_previous_cli)
CUSTOM_DOC("Same as execute_any_cli but outputs to the build panel.")
{
    String_Const_u8 out_buffer = SCu8(out_buffer_space);
    String_Const_u8 cmd = SCu8(global_cli_query.string[0]);
    String_Const_u8 hot_directory = SCu8(hot_directory_space);
    
    if (out_buffer.size > 0 && cmd.size > 0 && hot_directory.size > 0){
        Scratch_Block scratch(app);
        
        View_ID view = get_or_open_build_panel(app);
        Buffer_Identifier id = buffer_identifier(out_buffer);
        exec_system_command(app, view, id, hot_directory, cmd, CLI_OverlapWithConflict|CLI_CursorAtEnd|CLI_SendEndSignal);
        Buffer_ID buffer = view_get_buffer(app, view, Access_Always);
        String_Const_u8 command_title = push_u8_stringf(scratch, "Running %.*s:\n", string_expand(cmd));
        buffer_replace_range(app, buffer, Ii64(), command_title);
        lock_jump_buffer(app, out_buffer);
    }
}

CUSTOM_COMMAND_SIG(mod_execute_any_cli)
CUSTOM_DOC("Improved version of execute_any_cli."){
    b32 in_query = modal_active_mode_inherits_from(app, mode_query);
    
    if(!in_query) {
        Query_State *query = &global_cli_query;
        query_start(app, query);
        
        query->prompt[0] = string_u8_litexpr("Command: !");
        query->update_hook = 0;
        query->apply_hook  = mod_execute_any_cli;
        query->cancel_hook = 0;
    } else {
        Scratch_Block scratch(app);
        Query_State *query = active_query_state;
        
        String_Const_u8 command_name = string_skip_chop_whitespace(SCu8(query->string[0]));
        command_name = string_replace(scratch, command_name, string_u8_litexpr("/"), string_u8_litexpr("_"));
        command_name = string_replace(scratch, command_name, string_u8_litexpr("\\"), string_u8_litexpr("_"));
        
        String_u8 buffer_name = Su8(out_buffer_space, (u64)0, sizeof(out_buffer_space) - 1);
        string_append(&buffer_name, string_u8_litexpr("*CLI: "));
        string_append(&buffer_name, command_name);
        string_append(&buffer_name, string_u8_litexpr("*"));
        buffer_name.size = clamp_top(buffer_name.size, sizeof(out_buffer_space) - 1);
        out_buffer_space[buffer_name.size] = 0;
        
        String_Const_u8 hot = push_hot_directory(app, scratch);
        {
            u64 size = clamp_top(hot.size, sizeof(hot_directory_space));
            block_copy(hot_directory_space, hot.str, size);
            hot_directory_space[hot.size] = 0;
        }
        
        String_Const_u8 file_name = push_buffer_file_name(app, scratch, query->buffer);
        String_Const_u8 command = SCu8(query->string[0]);
        command = string_replace(scratch, command, string_u8_litexpr("%f"), file_name);
        command = string_replace(scratch, command, string_u8_litexpr("%F"), string_front_of_path(file_name));
        command = string_replace(scratch, command, string_u8_litexpr("%D"), push_hot_directory(app, scratch));
        command = string_replace(scratch, command, string_u8_litexpr("%d"), string_remove_front_of_path(file_name));
        command = string_replace(scratch, command, string_u8_litexpr("%%"), string_u8_litexpr("%"));
        
        u64 command_size = clamp_top(command.size, MOD_QUERY_CAP);
        block_copy(query->string[0].str, command.str, command_size);
        query->string[0].size = command_size;
        
        mod_execute_previous_cli(app);
    }
}

#endif // _MODAL_CLI_COMMANDS_CPP